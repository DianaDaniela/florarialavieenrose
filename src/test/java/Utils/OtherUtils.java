package Utils;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import java.util.Random;

public class OtherUtils {

    public static String sanitiseNullDbString(String dbResult) {
        if (dbResult == null) {
            return "";
        }
        return dbResult;
    }
    public static String getRandomString(int size) {
        final String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder results = new StringBuilder();
        for (int i = 0; i < size; i++) {
            Random rnd = new Random();
            results.append(alphabet.charAt(rnd.nextInt(alphabet.length())));
        }
        return results.toString();
    }

    public static String getRandomEmail() {
        String user = getRandomString(10);
        String domain = "gmail";
        String tld = "com";
        StringBuilder sb = new StringBuilder();
        //System.out.println(user);
        return sb.append(user).append("@").append(domain).append(".").append(tld).toString();
    }

    public static void logInfoStatus(ExtentTest test, String message) {
        if (test != null)
            test.log(Status.INFO, message);
        System.out.println(message);
    }

}
