package tests;

import Utils.SeleniumUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.ContactPage;

public class ContactTest extends BaseTest {

    //  Verifica existenta elementelor de contact in pagina
    @Test
    public void contact() {
        test = extent.createTest("Contact Test");
        ContactPage ct = new ContactPage(driver);
        ct.openContactPage(hostname);
        SeleniumUtils.clickAcceptCookies(driver);
        ct.checkContactPage();
        Assert.assertTrue(ct.checkElements(), "Date de Contact");
        logInfoStatus("Check contact info on page!");
    }

}
