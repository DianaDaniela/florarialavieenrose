package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.DespreNoiPage;

public class DespreNoiTest extends BaseTest {

    //    Testul verifica existenta linkului "DESPRE NOI" Si continutul paginii
    @Test
    public void povestea() {
        test = extent.createTest("Povestea Test");
        DespreNoiPage dpn = new DespreNoiPage(driver);
        dpn.openDespreNoiPage(hostname);
        Assert.assertTrue(dpn.checkEchipa(), "ECHIPA LA VIE EN ROSE");
        logInfoStatus("Verifica povestea echipei!");
    }
}
