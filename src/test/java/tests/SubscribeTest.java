package tests;

import Utils.SeleniumUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.SubscribePage;

public class SubscribeTest extends BaseTest {

    //Testul pentru subscribe cu data input e-mail address
    @Test
    public void subscribeNews() {
        test = extent.createTest("Subscribe Test");
        SubscribePage sp = new SubscribePage(driver);
        sp.openSubscribePage(hostname);
        SeleniumUtils.clickAcceptCookies(driver);
        sp.subscription("ioanapopa@yahoo.com");
        System.out.println(sp.confirmation());
        Assert.assertTrue(sp.checkSubscribe(), "Ne bucuram ca doriti sa primiti informatii despre noutatile noastre. " +
                "Va rugam sa verificati adresa de dvs de email pentru confirmare. Va multumim!");
        logInfoStatus("Successful subscription!");
    }
}
