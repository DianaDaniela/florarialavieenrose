package tests;

import Utils.SeleniumUtils;
import org.openqa.selenium.By;

import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.AddCartPage;


public class AddToCart extends BaseTest {
    @Test
    public void addTo() {
        test = extent.createTest("Add To Cart Test");
        AddCartPage add = new AddCartPage(driver);
        add.openAddCartPage(hostname);
        SeleniumUtils.waitForGenericElementClickable(driver, By.id("pa_dimensiune"), 200);
        Select options = new Select(driver.findElement(By.id("pa_dimensiune")));
        // Adauga 3 buchete de dimensiune "SMALL"
        options.selectByVisibleText("SMALL");
        add.editAddCartPage("3");
        Assert.assertTrue(add.checkCart(), "3 × „Luxury Collection – Cutie mix de flori pastel” au fost adăugate în coș.");
        logInfoStatus("Added to cart!");
    }
}
