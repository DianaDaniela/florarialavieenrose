package tests;

import Models.LoginModel;
import Utils.SeleniumUtils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.LoginPage;
import pageObjects.ProfilePage;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static Utils.OtherUtils.sanitiseNullDbString;

public class LogoutTest extends BaseTest {
    //    Data Provider pentru Logout cu citire date din fisier csv
    @DataProvider(name = "loginDp")
    public Iterator<Object[]> sqlDpCollection() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://" + dbHostname + ":" + dbPort + "/" + dbSchema,
                    dbUsername, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM lavieenrose.login;");
            while (results.next()) {
                LoginModel lm = new LoginModel();
                lm.setUsername(sanitiseNullDbString(results.getString("username")));
                lm.setPassword(sanitiseNullDbString(results.getString("password")));
                dp.add(new Object[]{lm});
            }
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp.iterator();
    }

    //    Test pentru Logout
    @Test(dataProvider = "loginDp")
    public void logoutTest(LoginModel lm) {
        test = extent.createTest("Logout  Test");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(hostname);
        SeleniumUtils.clickAcceptCookies(driver);
        loginPage.login(lm.getUsername(), lm.getPassword());
        ProfilePage profilePage = new ProfilePage(driver);
        Assert.assertEquals(lm.getUsername(), profilePage.getUser());
        System.out.println("Logged user: " + profilePage.getUser());
        profilePage.logOut();
        Assert.assertTrue(profilePage.checkLogout(), "VEZI BUCHETE");
    }
}
