package tests;


import Models.EditAccountModel;
import Utils.SeleniumUtils;
import com.opencsv.CSVReader;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.EditAccountPage;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


public class EditAccountTest extends BaseTest {

    //    Data Provider pentru editarea contului cu citire din fisier csv
    @DataProvider(name = "csvDp")
    public Iterator<Object[]> csvDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        File f = new File("src\\test\\resources\\data\\testdata.csv");
        Reader reader = Files.newBufferedReader(Paths.get(f.getAbsolutePath()));
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> csvData = csvReader.readAll();
        for (int i = 0; i < csvData.size(); i++) {
            dp.add(new Object[]{new EditAccountModel(csvData.get(i)[0],
                    csvData.get(i)[1], csvData.get(i)[2], csvData.get(i)[3],
                    csvData.get(i)[4], csvData.get(i)[5], csvData.get(i)[6], csvData.get(i)[7])});
        }
        return dp.iterator();
    }

    //    Testul editeaza contul clientului cu citire a datelor din fisier csv
    @Test(dataProvider = "csvDp")
    public void editAcc(EditAccountModel acm) {
        test = extent.createTest("Edit Account Test");
        EditAccountPage lp = new EditAccountPage(driver);
        lp.openEditAccPage(hostname);

        lp.editAccount(acm.getPrenume(), acm.getNume(), acm.getNumeDeAfisat(), acm.getEmail(),
                acm.getParolaActuala(), acm.getParolaNoua(), acm.getParolaConfirmata(), acm.getDateLivrare());
        System.out.println("Contul s-a actualizat pentru: " + acm.getPrenume() + " " + acm.getNume());
        Assert.assertTrue(lp.checkAcc());
        logInfoStatus("Edit account with input from csv file!");
    }
}


