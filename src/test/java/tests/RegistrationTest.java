package tests;

import Utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.RegisterPage;


public class RegistrationTest extends BaseTest {

    //Testul pentru registration cu data input e-mail address
    @Test
    public void registration() {
        test = extent.createTest("Registration Test");
        RegisterPage registrationPage = new RegisterPage(driver);
        registrationPage.openRegisterPage(hostname);
        SeleniumUtils.clickAcceptCookies(driver);
        registrationPage.register("Araiannapopa@yahoo.com");
        logInfoStatus("Register!");
//        Assert.assertTrue(registrationPage.checkRegistration(),"ÎNREGISTRARE");
    }
}
