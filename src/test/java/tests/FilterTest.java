package tests;

import Utils.SeleniumUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.FilterPage;

public class FilterTest extends BaseTest {
    // Filtrare  dupa "lalele"
    @Test
    public void filter() {
        test = extent.createTest("Filter Test");
        FilterPage fp = new FilterPage(driver);
        fp.openFilterPage(hostname);
        SeleniumUtils.clickAcceptCookies(driver);
        fp.filterSpring();
        Assert.assertTrue(fp.checkFilter(), "Afișez toate cele 10 rezultate");
        logInfoStatus("Filter about lalele!");
    }
}
