package tests;

import Utils.SeleniumUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.SearchPage;


public class SearchTest extends BaseTest {

    //Testul cauta dupa cuvinte inrudite si negative data testing
    @Test
    public void searchBar() {
        test = extent.createTest("Search Test");
        driver.get(hostname);
        SeleniumUtils.clickAcceptCookies(driver);
        SearchPage sp = new SearchPage(driver);
        sp.setSearch("lalele");
        Assert.assertTrue(sp.checkSearchPozitiv(),"SEARCH RESULTS FOR: LALELE");
        logInfoStatus("Search with related word!");
        sp.setSearchNeg("12+^$fdsd");
        Assert.assertTrue(sp.checkSearchNeg(),"NU A FOST GĂSIT NICIUN PRODUS CARE SĂ SE POTRIVEASCĂ CU SELECȚIA TA.");
        logInfoStatus("Search with unrelated word!");
    }

}
