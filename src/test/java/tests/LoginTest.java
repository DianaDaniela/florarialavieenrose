package tests;

import Models.LoginModel;
import Utils.SeleniumUtils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.LoginPage;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static Utils.OtherUtils.sanitiseNullDbString;

public class LoginTest extends BaseTest {
    // Data provider pentru Login cu citire credentiale din db
    @DataProvider(name = "loginDp")
    public Iterator<Object[]> sqlDpCollection() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://" + dbHostname + ":" + dbPort + "/" + dbSchema, dbUsername, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM lavieenrose.login;");
            while (results.next()) {
                LoginModel lm = new LoginModel();
                lm.setUsername(sanitiseNullDbString(results.getString("username")));
                lm.setPassword(sanitiseNullDbString(results.getString("password")));
                dp.add(new Object[]{lm});
            }
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp.iterator();
    }

    //    Testul de Login
    @Test(dataProvider = "loginDp")
    public void loginTest(LoginModel lm) {
        test = extent.createTest("Login Test");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(hostname);
        SeleniumUtils.clickAcceptCookies(driver);
        loginPage.login(lm.getUsername(), lm.getPassword());
        Assert.assertTrue(loginPage.checkLogin(), "Bună dianabardasan (nu ești dianabardasan? Dezautentifică-te)");
        logInfoStatus("Login with credentials from db!");
    }
}
