package Models;

public class EditAccountModel {
    private String prenume;
    private String nume;
    private String numeDeAfisat;
    private String email;
    private String parolaActuala;
    private String parolaNoua;
    private String parolaConfirmata;
    private String dateLivrare;


    public EditAccountModel(String prenume, String nume, String numeDeAfisat, String email, String parolaActuala,
                            String parolaNoua, String parolaConfirmata, String dateLivrare) {
        this.prenume = prenume;
        this.nume = nume;
        this.numeDeAfisat = numeDeAfisat;
        this.email = email;
        this.parolaActuala = parolaActuala;
        this.parolaNoua = parolaNoua;
        this.parolaConfirmata = parolaConfirmata;
        this.dateLivrare = dateLivrare;
    }


    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getNumeDeAfisat() {
        return numeDeAfisat;
    }

    public void setNumeDeAfisat(String numeDeAfisat) {
        this.numeDeAfisat = numeDeAfisat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getParolaActuala() {
        return parolaActuala;
    }

    public void setParolaActuala(String parolaActuala) {
        this.parolaActuala = parolaActuala;
    }

    public String getParolaNoua() {
        return parolaNoua;
    }

    public void setParolaNoua(String parolaNoua) {
        this.parolaNoua = parolaNoua;
    }

    public String getParolaConfirmata() {
        return parolaConfirmata;
    }

    public void setParolaConfirmata(String parolaConfirmata) {
        this.parolaConfirmata = parolaConfirmata;
    }

    public String getDateLivrare() {
        return dateLivrare;
    }

    public void setDateLivrare(String dateLivrare) {
        this.dateLivrare = dateLivrare;
    }
}
