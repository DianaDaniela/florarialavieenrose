package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContactPage {
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//*[@id=\"post-813\"]/div/div[1]/div/div/div/div[2]/div[2]/div[1]/div/div/div/div/p")
    WebElement contactAddress;
    @FindBy(xpath = "//*[@id=\"post-813\"]/div/div[3]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div[2]/div/p/a")
    WebElement telefon1;
    @FindBy(xpath = "//*[@id=\"post-813\"]/div/div[3]/div[2]/div/div/div[2]/div[1]/div/div/div[2]/div[2]/div/p")
    WebElement telefon2;
    @FindBy(xpath = "//*[@id=\"post-813\"]/div/div[3]/div[2]/div/div/div[2]/div[2]/div/div/div[1]/div[2]/div/p/a")
    WebElement emailAddress;
    @FindBy(className = "title")
    WebElement dateDeContact;

    public ContactPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void checkContactPage() {
        System.out.println("Adresa si programul florariei sunt: " + contactAddress.getText() + ", numerele de telefon sunt: "
                + telefon1.getText() + " si " + telefon2.getText() + " iar adresa de email este: " + emailAddress.getText());
    }

    public boolean checkElements() {
        return dateDeContact.isDisplayed();

    }

    public void openContactPage(String hostname) {
        driver.get(hostname + "/contact/");
    }
}
