package pageObjects;

import Utils.OtherUtils;
import Utils.SeleniumUtils;
import org.openqa.selenium.*;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public class EditAccountPage {
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(id = "account_first_name")
    WebElement prenumeInput;
    @FindBy(id = "account_last_name")
    WebElement numeInput;
    @FindBy(id = "account_display_name")
    WebElement numeDeAfisatInput;
    @FindBy(id = "account_email")
    WebElement emailInput;
    @FindBy(id = "password_current")
    WebElement parolaActualaInput;
    @FindBy(id = "password_1")
    WebElement parolaNouaInput;
    @FindBy(id = "password_2")
    WebElement parolaConfirmataInput;
    @FindBy(id = "shipping_date")
    WebElement dateLivrareInput;
    @FindBy(xpath = "//*[@id=\"ui-datepicker-div\"]/div[2]/button[1]")
    WebElement dataHendler;
    @FindBy(xpath = "//*[@id=\"post-12\"]/div/div/div/div[2]/form/p[6]/button")
    WebElement salveazaModificarile;

    @FindBy(xpath = "//*[@id=\"post-12\"]/div/div/div/div[2]/div/ul/li[1]")
    WebElement prenumeErr;
    @FindBy(xpath = "//*[@id=\"post-12\"]/div/div/div/div[2]/div/ul/li[2]")
    WebElement numeErr;
    @FindBy(xpath = "//*[@id=\"post-12\"]/div/div/div/div[2]/div/ul/li[3]")
    WebElement dataErr;

    public EditAccountPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void editAccount(String prenume, String nume, String numeAfisat, String email,
                            String parola, String parolaNoua, String parolaConfirmata, String dateLivrare) {
        prenumeInput.clear();
        prenumeInput.sendKeys(prenume);
        numeInput.clear();
        numeInput.sendKeys(nume);
        numeDeAfisatInput.clear();
        numeDeAfisatInput.sendKeys(numeAfisat);
        emailInput.clear();
        emailInput.sendKeys(email);
        parolaActualaInput.clear();
        parolaActualaInput.sendKeys(parola);
        parolaNouaInput.clear();
        parolaNouaInput.sendKeys(parolaNoua);
        parolaConfirmataInput.clear();
        parolaConfirmataInput.sendKeys(parolaConfirmata);
        dateLivrareInput.sendKeys(dateLivrare);
        dateLivrareInput.click();
        salveazaModificarile.submit();
    }

    public void openEditAccPage(String hostname) {
        driver.get(hostname + "/contul-meu/");
        WebElement autentificare = driver.findElement(By.xpath("//*[@id=\"customer_login\"]/div[3]/a"));
        autentificare.click();
        WebElement email = driver.findElement(By.id("reg_email"));
        email.sendKeys(OtherUtils.getRandomEmail());
        //Accept cookies
        SeleniumUtils.clickAcceptCookies(driver);
        WebElement inregistrare = SeleniumUtils.scrollUntilElementVisible(driver, By.xpath("//*[@id='customer_login']/div[2]/form/p[3]/button"));
        inregistrare.click();
        //Trebuie sa te intorci la my account
        WebElement myAccount = SeleniumUtils.waitForGenericElementClickable(driver, By.xpath("//*[@id='menu-item-294']/a/i"), 10);
        myAccount.click();
        WebElement detaliiCont = driver.findElement(By.xpath("//*[@id=\"post-12\"]/div/div/div/div[1]/nav/ul/li[5]/a"));
        detaliiCont.click();
    }

    public boolean checkAcc() {
        return numeInput.isDisplayed();
    }
}
