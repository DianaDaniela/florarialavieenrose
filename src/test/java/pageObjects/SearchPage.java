package pageObjects;

import Utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage {
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "/html/body/div[4]/header/div[1]/div/div[3]/div[2]/a")
    WebElement search;
    @FindBy(xpath = "/html/body/div[4]/header/div[1]/div/div[3]/div[2]/div/div/form/div/input[1]")
    WebElement cauta;
    @FindBy(className = "woocommerce-result-count")
    WebElement rezultateCautari;
    @FindBy(className = "woocommerce-info")
    WebElement rezultatCautariNeg;
    @FindBy(className = "entry-title")
    WebElement entryTitle;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void setSearch(String sendKeys) {
        SeleniumUtils.waitForGenericElementClickable(driver, By.xpath("/html/body/div[4]/header/div[1]/div/div[3]/div[2]/a"), 100);
        search.click();
        SeleniumUtils.waitForGenericElementClickable(driver, By.xpath("/html/body/div[4]/header/div[1]/div/div[3]/div[2]/div/div/form/div/input[1]"), 300);
        cauta.sendKeys(sendKeys);
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.ENTER).build().perform();
        System.out.println(rezultateCautari.getText());
    }

    public void setSearchNeg(String sendKeys) {
        SeleniumUtils.waitForGenericElementClickable(driver, By.xpath("/html/body/div[4]/header/div[1]/div/div[3]/div[2]/a"), 100);
        search.click();
        SeleniumUtils.waitForGenericElementClickable(driver, By.xpath("/html/body/div[4]/header/div[1]/div/div[3]/div[2]/div/div/form/div/input[1]"), 300);
        cauta.clear();
        cauta.sendKeys(sendKeys);
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.ENTER).build().perform();
        System.out.println(rezultatCautariNeg.getText());
    }

    public boolean checkSearchPozitiv() {
        return entryTitle.isDisplayed();
    }

    public boolean checkSearchNeg() {
        return rezultatCautariNeg.isDisplayed();
    }
}
