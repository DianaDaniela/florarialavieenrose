package pageObjects;

import Utils.SeleniumUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DespreNoiPage {
    WebDriver driver;
    WebDriverWait wait;
     @FindBy(xpath = "//*[@id=\"menu-item-10566\"]/a")
    WebElement despreNoi;
     @FindBy(xpath = "//*[@id=\"post-781\"]/div/div[3]/div[1]/div/div/div[1]/div/h1")
    WebElement echipa;

    public DespreNoiPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void openDespreNoiPage(String hostname) {
        driver.get(hostname + "/povestea-noastra/");
        SeleniumUtils.clickAcceptCookies(driver);
        despreNoi.click();
    }

    public boolean checkEchipa() {
        return echipa.isDisplayed();

    }
}
