package pageObjects;
;
import Utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public class RegisterPage {
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//*[@id=\"customer_login\"]/div[3]/a")
    WebElement inregistrare1;
    @FindBy(id = "reg_email")
    WebElement adresaEmailInput;
    @FindBy(xpath = "//*[@id=\"customer_login\"]/div[2]/form/p[3]/button")
    WebElement inregistrare2;
    @FindBy(className = "register-or")
    WebElement register;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void register(String emailadresa) {
        adresaEmailInput.clear();
        adresaEmailInput.sendKeys(emailadresa);
        inregistrare2.submit();
    }

    public void openRegisterPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/contul-meu/");
        driver.get(hostname + "/contul-meu/");
        inregistrare1.click();
        SeleniumUtils.waitForGenericElement(driver, By.className("register-or"), 200);
    }

    public boolean checkRegistration() {
        return register.isDisplayed();
    }
}
