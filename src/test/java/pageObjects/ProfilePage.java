package pageObjects;

import Utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProfilePage {
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(className = "logout-link")
    WebElement logOutButton;
    @FindBy(xpath = "//*[@id=\"cumpara_home_banner\"]/div/div/a")
    WebElement veziBuchete;


    By user = By.xpath("//*[@id=\"basel-user-panel-17\"]/div/div[2]/span/strong");

    public ProfilePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(this.driver, this);
    }

    public void logOut() {
        wait.until(ExpectedConditions.elementToBeClickable(logOutButton)).click();
    }

    public String getUser() {
        return SeleniumUtils.waitForGenericElement(driver, user, 15).getText();
    }

    public boolean checkLogout() {
        return veziBuchete.isDisplayed();
    }
}
