package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SubscribePage {
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//*[@id=\"mc4wp-form-1\"]/div[1]/p[1]/input")
    WebElement emailInput;
    @FindBy(xpath = "//*[@id=\"mc4wp-form-1\"]/div[1]/p[2]/input")
    WebElement abonare;
    @FindBy(xpath = "//*[@id=\"mc4wp-form-1\"]/div[2]/div/p")
    WebElement message;
    @FindBy(xpath = "//*[@id=\"mc4wp-form-1\"]/div[2]/div")
    WebElement subscriereSucces;

    public SubscribePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void subscription(String emailAddress) {
        emailInput.sendKeys(emailAddress);
        abonare.submit();
        message.getText();
    }

    public String confirmation() {
        return message.getText();
    }

    public void openSubscribePage(String hostname) {
        driver.get(hostname);
    }

    public boolean checkSubscribe() {
        return subscriereSucces.isDisplayed();
    }
}

