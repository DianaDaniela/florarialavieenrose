package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(id = "username")
    WebElement userEmailInput;
    @FindBy(id = "password")
    WebElement passwordInput;
    @FindBy(name = "login")
    WebElement loginButton;
    @FindBy(xpath = "//*[@id=\"cumpara_home_banner\"]/div/div/a")
    WebElement veziBuchete;
    @FindBy(className = "user-info")
    WebElement userInfo;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void login(String username, String password) {
        userEmailInput.clear();
        userEmailInput.sendKeys(username);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        loginButton.click();
    }

    public void waitForLoginPage() {
        wait.until(ExpectedConditions.elementToBeClickable(veziBuchete));
    }


    public void openLoginPage(String hostname) {
        System.out.println("Open with nest url: " + hostname + "/contul-meu/?action=login");
        driver.get(hostname + "/contul-meu/?action=login");
    }

    public boolean checkLogin(){
        return userInfo.isDisplayed();

    }
}
