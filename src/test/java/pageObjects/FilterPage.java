package pageObjects;

import Utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FilterPage {
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//*[@id=\"woocommerce_product_categories-2\"]/ul/li[7]/a")
    WebElement spring;
    @FindBy(className = "open-filters")
    WebElement filters;
    @FindBy(xpath = "//*[@id=\"BASEL_Widget_Price_Filter\"]/div/ul/li[1]/a")
    WebElement all;
    @FindBy(className = "woocommerce-result-count")
    WebElement result;
    @FindBy(xpath = "//*[@id=\"menu-item-1906\"]/a")
    WebElement bucheteFlori;

    public FilterPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void filterSpring() {
        spring.click();
        SeleniumUtils.waitForGenericElementClickable(driver, By.className("open-filters"), 100);
        filters.click();
        SeleniumUtils.waitForGenericElementClickable(driver, By.xpath("//*[@id=\"BASEL_Widget_Price_Filter\"]/div/ul/li[1]/a"), 100);
        all.click();
        SeleniumUtils.waitForGenericElementClickable(driver, By.className("woocommerce-result-count"), 100);
        result.getText();
        System.out.println("Rezultatul este: " + result.getText());
    }

    public void openFilterPage(String hostname) {
        driver.get(hostname + "/buchete-de-flori/");
    }

    public boolean checkFilter() {
        return result.isDisplayed();
    }
}
