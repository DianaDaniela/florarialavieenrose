package pageObjects;

import Utils.SeleniumUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddCartPage {
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "/html/body/div[4]/div[4]/div/div/div[1]/div[3]/div[1]/div/div/div/div[2]/div/div/form/div/div[2]/div[2]/input[2]")
    WebElement nrInput;
    @FindBy(xpath = "//*[@id=\"product-11319\"]/div[1]/div/div/div/div[2]/div/div/form/div/div[2]/button")
    WebElement adauga;

    @FindBy(xpath = "/html/body/div[4]/div[4]/div/div/div[1]/div[1]")
    WebElement mesaj;
    @FindBy(id = "pa_dimensiune")
    WebElement adaugaOptiune;

    public AddCartPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void editAddCartPage(String nr) {
        nrInput.clear();
        nrInput.sendKeys(nr);
        adauga.submit();
    }

    public boolean checkCart() {
        return mesaj.isDisplayed();
    }

    public void openAddCartPage(String hostname) {
        driver.get(hostname + "/everyday-collection-buchet-zambile-colorate/");
        SeleniumUtils.clickAcceptCookies(driver);
        adaugaOptiune.click();
    }
}
